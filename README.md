# hid-sony

Updated driver for the Sony Dualshock 4.

* Map the touchpad button to SELECT
* Map the share button to F12
* Disable gyro controls
* Disable the touchpad
* Reduce intensity of the LEDs
