#/usr/bin/make
SRC = $(DESTDIR)/usr/src
PACKAGE_VERSION=1
PACKAGE_NAME=hid-sony-torkel104

all:
	@echo Generating dkms.conf
	@echo PACKAGE_VERSION=\"${PACKAGE_VERSION}\" > dkms.conf
	@echo PACKAGE_NAME=\"${PACKAGE_NAME}\" >> dkms.conf
	@cat dkms.conf.template >> dkms.conf

clean:
	rm dkms.conf

install: all
	install -m 0644 -D -t $(SRC)/$(PACKAGE_NAME)-$(PACKAGE_VERSION) src/* dkms.conf COPYING
	dkms add -m $(PACKAGE_NAME) -v $(PACKAGE_VERSION)
	dkms build -m $(PACKAGE_NAME) -v $(PACKAGE_VERSION)
	dkms install -m $(PACKAGE_NAME) -v $(PACKAGE_VERSION)

remove:
	dkms remove -m $(PACKAGE_NAME) -v $(PACKAGE_VERSION) --all
	rm -rf $(SRC)/$(PACKAGE_NAME)-$(PACKAGE_VERSION)
